---
title: kernel-xanmod updates for February 10th, 2022
date: 2022-02-10
summary: Updates for the kernel-xanmod project
description: ※ This page is autogenerated
tags: ['news','kernel-xanmod']
---

### NEW RELEASE FOR PACKAGE `kernel-xanmod-edge`+`kernel-xanmod-exptl`

Changes for 5.16.8-xm1.0 + 5.16.8-xm1.0e20211105:
(※ Refer commit tags to https://github.com/xanmod/linux):
- 9995a354b3b9 Linux 5.16.8-xanmod1
- 5291c3213ba8 Merge tag 'v5.16.8' into 5.16
- df659ebe5d2f Linux 5.16.8
- 0efb43eb32a1 selftests: netfilter: check stateless nat udp checksum fixup
- aa8fa75e15b2 selftests: nft_concat_range: add test for reload with no element add/del
- bc3d8a98c0c8 gpio: mpc8xxx: Fix an ignored error return from platform_get_irq()
- 54e8d33ea77d gpio: idt3243x: Fix an ignored error return from platform_get_irq()
- 410696817b15 tools include UAPI: Sync sound/asound.h copy with the kernel sources
- bd69089ce5b1 cgroup/cpuset: Fix "suspicious RCU usage" lockdep warning
- 6506bb5b1c44 net: dsa: mt7530: make NET_DSA_MT7530 select MEDIATEK_GE_PHY
- a17cde2d2e75 ext4: fix incorrect type issue during replay_del_range
- 14aa3f49c7fc ext4: fix error handling in ext4_fc_record_modified_inode()
- 627b44cf5543 ext4: fix error handling in ext4_restore_inline_data()
- 3ecbe2f8eb27 ext4: modify the logic of ext4_mb_new_blocks_simple
- 33fb59317cd2 ext4: prevent used blocks from being allocated during fast commit replay
- 11dfe20342f0 EDAC/xgene: Fix deferred probing
- 9a258cb56936 EDAC/altera: Fix deferred probing
- a90f9f3e0a2a x86/perf: Default set FREEZE_ON_SMI for all
- feffb6ae2c80 perf/x86/intel/pt: Fix crash with stop filters in single-range mode
- c5d62c82be1d perf stat: Fix display of grouped aliased events
- 8ee67e3c34bc perf: Copy perf_event_attr::sig_data on modification
- 5f780b1435f5 kvm/arm64: rework guest entry logic
- bfa75faeb7ea kvm: add guest_state_{enter,exit}_irqoff()
- f5b14654ae1b objtool: Fix truncated string warning
- d0a9662105c1 rtc: cmos: Evaluate century appropriate
- f96cdab32299 e1000e: Separate ADP board type from TGP
- fbc83aeccd80 tools/resolve_btfids: Do not print any commands when building silently
- e2242343ba19 selftests: futex: Use variable MAKE instead of make
- 191edea5db19 selftests/exec: Remove pipe from TEST_GEN_FILES
- d578933f6226 bpf: Use VM_MAP instead of VM_ALLOC for ringbuf
- 7d9c47013410 gve: fix the wrong AdminQ buffer queue index check
- 7e7e81aa81e0 nfsd: nfsd4_setclientid_confirm mistakenly expires confirmed client.
- 2d24336c7214 scsi: bnx2fc: Make bnx2fc_recv_frame() mp safe
- 676c0f155ad2 btrfs: fix use of uninitialized variable at rm device ioctl
- be064d88cdd4 pinctrl: bcm2835: Fix a few error paths
- 4519e6fc7174 pinctrl: intel: fix unexpected interrupt
- d0e869bcdfe9 pinctrl: intel: Fix a glitch when updating IRQ flags on a preconfigured line
- 68b70ac68a91 pinctrl: sunxi: Fix H616 I2S3 pin data
- 83775594b450 ASoC: qdsp6: q6apm-dai: only stop graphs that are started
- 8a2812b4aea1 ASoC: codecs: wcd938x: fix return value of mixer put function
- 4bb0bd2e7936 ASoC: codecs: lpass-rx-macro: fix sidetone register offsets
- 9167f2712dc8 ASoC: codecs: wcd938x: fix incorrect used of portid
- f114fd6165df ASoC: max9759: fix underflow in speaker_gain_control_put()
- c6ede1d4455e ASoC: cpcap: Check for NULL pointer after calling of_get_child_by_name
- 6a2ae1bc2ccd ASoC: simple-card: fix probe failure on platform component
- aa610c4f1771 ASoC: xilinx: xlnx_formatter_pcm: Make buffer bytes multiple of period bytes
- f5cfb8c53926 ASoC: fsl: Add missing error handling in pcm030_fabric_probe
- a2b79c1594c4 ASoC: rt5682: Fix deadlock on resume
- bd741337c284 drm/amd: avoid suspend on dGPUs w/ s2idle support when runtime PM enabled
- 855f1528f4c5 drm/i915: Lock timeline mutex directly in error path of eb_pin_timeline
- 76aad713eb86 drm/i915/overlay: Prevent divide by zero bugs in scaling
- cb64de55ed11 drm/kmb: Fix for build errors with Warray-bounds
- 6f9267e01cca drm: mxsfb: Fix NULL pointer dereference
- c16fb9ebbc52 net: stmmac: ensure PTP time register reads are consistent
- 203a35ebb49c net, neigh: Do not trigger immediate probes on NUD_FAILED from neigh_managed_work
- 40d20d9a0257 net: stmmac: dump gmac4 DMA registers correctly
- 40c80a3fd42b net: macsec: Verify that send_sci is on when setting Tx sci explicitly
- 8299be160aad net: macsec: Fix offload support for NETDEV_UNREGISTER event
- 5dc4be3b9ae3 net: stmmac: properly handle with runtime pm in stmmac_dvr_remove()
- 262c05aae6c4 net: stmmac: dwmac-visconti: No change to ETHER_CLOCK_SEL for unexpected speed request.
- 504078fbe9dd net/smc: Forward wakeup to smc socket waitqueue after fallback
- e2e9d2e7c891 net: ieee802154: Return meaningful error codes from the netlink helpers
- d2295407c11f netfilter: nft_reject_bridge: Fix for missing reply from prerouting
- 21feb6df3967 net: ieee802154: ca8210: Stop leaking skb's
- 81704045967c net: ieee802154: mcr20a: Fix lifs/sifs periods
- 03b0f8590ad1 net: ieee802154: hwsim: Ensure proper channel selection at probe time
- 8e3b022d3d4e IB/cm: Release previously acquired reference counter in the cm_id_priv
- c2f79ff2180d IB/hfi1: Fix tstats alloc and dealloc
- 447c3d4046d7 spi: uniphier: fix reference count leak in uniphier_spi_probe()
- af6825eb7da8 spi: meson-spicc: add IRQ check in meson_spicc_probe
- a05b7dff542e spi: mediatek: Avoid NULL pointer crash in interrupt
- 6279623e4e0a spi: bcm-qspi: check for valid cs before applying chip select
- 0be365dbdcda iommu/amd: Fix loop timeout issue in iommu_ga_log_enable()
- b62eceb5f8f0 iommu/vt-d: Fix potential memory leak in intel_setup_irq_remapping()
- f2c290e7f081 ALSA: hda: Skip codec shutdown in case the codec is not registered
- 806943fe72e3 ALSA: hda: Fix signedness of sscanf() arguments
- 95cb31766018 ALSA: usb-audio: initialize variables that could ignore errors
- 6c6c2b03076c RDMA/mlx4: Don't continue event handler after memory allocation failure
- 2702b466c204 RDMA/siw: Fix broken RDMA Read Fence/Resume logic.
- 8218ef38aefb IB/rdmavt: Validate remote_addr during loopback atomic tests
- fa3b844a5084 RDMA/siw: Fix refcounting leak in siw_create_qp()
- ee2477e8ccd3 RDMA/ucma: Protect mc during concurrent multicast leaves
- 3365f9daa2a7 RDMA/cma: Use correct address when leaving multicast group
- a64abe188bc5 arm64: Add Cortex-A510 CPU part definition
- 50fefe7077e9 KVM: arm64: Stop handle_exit() from handling HVC twice when an SError occurs
- 57e2986c3b25 KVM: arm64: Avoid consuming a stale esr value when SError occur
- 5e7161c9d671 RISC-V: KVM: make CY, TM, and IR counters accessible in VU mode
- 086daee38087 Revert "ASoC: mediatek: Check for error clk pointer"
- 089068470e6a mptcp: fix msk traversal in mptcp_nl_cmd_set_flags()
- 72c4cec1d21a fbcon: Add option to enable legacy hardware acceleration
- ba724328faff Revert "fbcon: Disable accelerated scrolling"
- 0e90cb3f319c Revert "fbdev: Garbage collect fbdev scrolling acceleration, part 1 (from TODO list)"
- 1899c3cad265 IB/hfi1: Fix AIP early init panic
- 2d0587a70e53 IB/hfi1: Fix alloc failure with larger txqueuelen
- 1530d84fba1e IB/hfi1: Fix panic with larger ipoib send_queue_size
- cc8f7940d9c2 dma-buf: heaps: Fix potential spectre v1 gadget
- 6b064a42060f cifs: fix workstation_name for multiuser mounts
- 464b609dc937 block: bio-integrity: Advance seed correctly for larger interval sizes
- cebb0aceb21a mm/kmemleak: avoid scanning potential huge holes
- d52d73af59b1 mm/pgtable: define pte_index so that preprocessor could recognize it
- 29183f6dbcd1 mm/debug_vm_pgtable: remove pte entry from the page table
- 73fad64378e5 nvme-fabrics: fix state check in nvmf_ctlr_matches_baseopts()
- 8e34ac596242 drm/amd/display: Force link_rate as LINK_RATE_RBR2 for 2018 15" Apple Retina panels
- f4b4b70ba1e7 drm/amd/display: watermark latencies is not enough on DCN31
- bd6e1b930e12 drm/amd/display: Update watermark values for DCN301
- b39841ec54b5 drm/amd/pm: correct the MGpuFanBoost support for Beige Goby
- c51c6f12af5d drm/amdgpu: fix a potential GPU hang on cyan skillfish
- 273832450f59 drm/i915/adlp: Fix TypeC PHY-ready status readout
- e7c36fa8a1e6 drm/nouveau: fix off by one in BIOS boundary checking
- d99d14b169a1 Revert "fs/9p: search open fids first"
- 9372fa1d73da btrfs: fix use-after-free after failure to create a snapshot
- 31198e58c09e btrfs: fix deadlock between quota disable and qgroup rescan worker
- 65acdfdedc8b btrfs: don't start transaction for scrub if the fs is mounted read-only
- 02b554bd2f30 ata: libata-core: Introduce ATA_HORKAGE_NO_LOG_DIR horkage
- e100d548069f ALSA: hda/realtek: Fix silent output on Gigabyte X570 Aorus Xtreme after reboot from Windows
- 123be3664836 ALSA: hda/realtek: Fix silent output on Gigabyte X570S Aorus Master (newer chipset)
- a41a88de0a36 ALSA: hda/realtek: Add missing fixup-model entry for Gigabyte X570 ALC1220 quirks
- 558d4b1b88bd ALSA: hda/realtek: Add quirk for ASUS GU603
- 1dcfc5eeaba3 ALSA: hda: realtek: Fix race at concurrent COEF updates
- 813e9f3e06d2 ALSA: hda: Fix UAF of leds class devs at unbinding
- 343799268777 ALSA: usb-audio: Correct quirk for VF0770
- e09cf398e8c6 ASoC: ops: Reject out of bounds values in snd_soc_put_xr_sx()
- ef6cd9eeb380 ASoC: ops: Reject out of bounds values in snd_soc_put_volsw_sx()
- bb72d2dda855 ASoC: ops: Reject out of bounds values in snd_soc_put_volsw()
- 1552e66be325 ASoC: hdmi-codec: Fix OOB memory accesses
- 049b27e185e9 spi: stm32-qspi: Update spi registering
- 3b6b7cbe466d ipc/sem: do not sleep with a spin lock held
- 2e45c87df389 audit: improve audit queue handling when "audit=1" on cmdline
- 7ed9cbf7ac0d selinux: fix double free of cond_list on error paths
- 8c7fa170e47f drm/i915: Disable DSB usage for now

---

### NEW RELEASE FOR PACKAGE `kernel-xanmod-tt`

Changes for 5.15.22-xm1.0:
(※ Refer commit tags to https://github.com/xanmod/linux):
- dc4d26227353 Linux 5.15.22-xanmod1-tt
- b79a235d8286 Merge branch '5.15' into 5.15-tt
- 995f210c75b3 Merge tag 'v5.15.22' into 5.15
- 0bf5b7cc9848 Linux 5.15.22
- 3853c4e27149 selftests: netfilter: check stateless nat udp checksum fixup
- b84753200e79 selftests: nft_concat_range: add test for reload with no element add/del
- 7c0ee51fe998 gpio: mpc8xxx: Fix an ignored error return from platform_get_irq()
- 3d631a1af0d7 gpio: idt3243x: Fix an ignored error return from platform_get_irq()
- ff43b75eea32 tools include UAPI: Sync sound/asound.h copy with the kernel sources
- f5afdefe188e cgroup/cpuset: Fix "suspicious RCU usage" lockdep warning
- e4a7e1418ab7 net: dsa: mt7530: make NET_DSA_MT7530 select MEDIATEK_GE_PHY
- f187daed64e1 ext4: fix incorrect type issue during replay_del_range
- 1b6762ecdf3c ext4: fix error handling in ext4_fc_record_modified_inode()
- ce38bb98cec7 ext4: fix error handling in ext4_restore_inline_data()
- 869cb287d893 ext4: modify the logic of ext4_mb_new_blocks_simple
- 0cb4480bc4f4 ext4: prevent used blocks from being allocated during fast commit replay
- d583cb17ee50 EDAC/xgene: Fix deferred probing
- c6c04bb32b73 EDAC/altera: Fix deferred probing
- 587dadb43c9a x86/perf: Default set FREEZE_ON_SMI for all
- e83d941fd344 perf/x86/intel/pt: Fix crash with stop filters in single-range mode
- 0f4dcaeaf6ae perf stat: Fix display of grouped aliased events
- 64e133ce280b perf: Copy perf_event_attr::sig_data on modification
- 9a60e92b76d6 kvm/arm64: rework guest entry logic
- 83071e2dad68 kvm: add guest_state_{enter,exit}_irqoff()
- aba976f96bbc rtc: cmos: Evaluate century appropriate
- 3b5fcdfab628 e1000e: Separate ADP board type from TGP
- b3a4d501e91c tools/resolve_btfids: Do not print any commands when building silently
- 7620887a7779 selftests: futex: Use variable MAKE instead of make
- c5610494fd45 selftests/exec: Remove pipe from TEST_GEN_FILES
- 5e457aeab52a bpf: Use VM_MAP instead of VM_ALLOC for ringbuf
- 62ab929a8f6b gve: fix the wrong AdminQ buffer queue index check
- 3611f4f91e11 nfsd: nfsd4_setclientid_confirm mistakenly expires confirmed client.
- 2f5a1ac68bdf scsi: bnx2fc: Make bnx2fc_recv_frame() mp safe
- 0bb4c6b9ecff pinctrl: bcm2835: Fix a few error paths
- 3cdcfa3c526e pinctrl: intel: fix unexpected interrupt
- ca63438dc5c4 pinctrl: intel: Fix a glitch when updating IRQ flags on a preconfigured line
- d4036172ca3b pinctrl: sunxi: Fix H616 I2S3 pin data
- f00012885e70 ASoC: codecs: wcd938x: fix return value of mixer put function
- b54ff87a1567 ASoC: codecs: lpass-rx-macro: fix sidetone register offsets
- aa7152f9f117 ASoC: codecs: wcd938x: fix incorrect used of portid
- baead410e5db ASoC: max9759: fix underflow in speaker_gain_control_put()
- 263b947aa4c1 ASoC: cpcap: Check for NULL pointer after calling of_get_child_by_name
- 841e6a6b831b ASoC: simple-card: fix probe failure on platform component
- c6cf5b5078db ASoC: xilinx: xlnx_formatter_pcm: Make buffer bytes multiple of period bytes
- 9d44f73df070 ASoC: fsl: Add missing error handling in pcm030_fabric_probe
- 8a15ac1786c9 drm/amd: avoid suspend on dGPUs w/ s2idle support when runtime PM enabled
- 5cba71707f0a drm/i915/overlay: Prevent divide by zero bugs in scaling
- b2c91bee7970 drm/kmb: Fix for build errors with Warray-bounds
- a84854bc230a net: stmmac: ensure PTP time register reads are consistent
- 27ea34ead54a net: stmmac: dump gmac4 DMA registers correctly
- 0ced878998f2 net: macsec: Verify that send_sci is on when setting Tx sci explicitly
- e7a0b3a0806d net: macsec: Fix offload support for NETDEV_UNREGISTER event
- 2967b08119d5 net: stmmac: properly handle with runtime pm in stmmac_dvr_remove()
- 6358e093547c net: stmmac: dwmac-visconti: No change to ETHER_CLOCK_SEL for unexpected speed request.
- 0ef6049f6649 net/smc: Forward wakeup to smc socket waitqueue after fallback
- ea8ecd2d65b4 net: ieee802154: Return meaningful error codes from the netlink helpers
- 566bf0e1c761 netfilter: nft_reject_bridge: Fix for missing reply from prerouting
- 94cd597e20ed net: ieee802154: ca8210: Stop leaking skb's
- 6c6b19a99131 net: ieee802154: mcr20a: Fix lifs/sifs periods
- 29e60b77a449 net: ieee802154: hwsim: Ensure proper channel selection at probe time
- bb7a226780e2 IB/cm: Release previously acquired reference counter in the cm_id_priv
- 40e20ba90390 IB/hfi1: Fix tstats alloc and dealloc
- dd00b4f8f768 spi: uniphier: fix reference count leak in uniphier_spi_probe()
- 66606d329d61 spi: meson-spicc: add IRQ check in meson_spicc_probe
- 7352f2c26482 spi: mediatek: Avoid NULL pointer crash in interrupt
- 101a1cf8af55 spi: bcm-qspi: check for valid cs before applying chip select
- ca1f48c30e5b iommu/amd: Fix loop timeout issue in iommu_ga_log_enable()
- 336d096b62bd iommu/vt-d: Fix potential memory leak in intel_setup_irq_remapping()
- e4b74b89862c ALSA: hda: Skip codec shutdown in case the codec is not registered
- 0c5c64335657 ALSA: hda: Fix signedness of sscanf() arguments
- c2a91f1ef38a ALSA: usb-audio: initialize variables that could ignore errors
- 63c69c93d978 RDMA/mlx4: Don't continue event handler after memory allocation failure
- 035ea99c537d RDMA/siw: Fix broken RDMA Read Fence/Resume logic.
- 7d9ad6f9f38f IB/rdmavt: Validate remote_addr during loopback atomic tests
- 2989ba9532ba RDMA/siw: Fix refcounting leak in siw_create_qp()
- 2923948ffe08 RDMA/ucma: Protect mc during concurrent multicast leaves
- 7715682f357d RDMA/cma: Use correct address when leaving multicast group
- 0452c3dc851b KVM: arm64: Stop handle_exit() from handling HVC twice when an SError occurs
- e1e852746997 KVM: arm64: Avoid consuming a stale esr value when SError occur
- aff6657f5243 Revert "ASoC: mediatek: Check for error clk pointer"
- 9908c759a17e mptcp: fix msk traversal in mptcp_nl_cmd_set_flags()
- 778283dc2840 fbcon: Add option to enable legacy hardware acceleration
- 2a2629db4248 Revert "fbcon: Disable accelerated scrolling"
- a3dd4d2682f2 IB/hfi1: Fix AIP early init panic
- 24f8e12d965b dma-buf: heaps: Fix potential spectre v1 gadget
- f576721152fd block: bio-integrity: Advance seed correctly for larger interval sizes
- a5389c80992f mm/kmemleak: avoid scanning potential huge holes
- 65a4863a4ed5 mm/pgtable: define pte_index so that preprocessor could recognize it
- 120973e64db9 mm/debug_vm_pgtable: remove pte entry from the page table
- 90391ac6888e nvme-fabrics: fix state check in nvmf_ctlr_matches_baseopts()
- 2093ecf557e7 drm/amd/display: Force link_rate as LINK_RATE_RBR2 for 2018 15" Apple Retina panels
- 7ff0ed88e4eb drm/amd/display: watermark latencies is not enough on DCN31
- 4f4c77ad5a13 drm/amd/pm: correct the MGpuFanBoost support for Beige Goby
- 39ac3945d966 drm/i915/adlp: Fix TypeC PHY-ready status readout
- d877e814a62b drm/nouveau: fix off by one in BIOS boundary checking
- b9e9f848c82b Revert "fs/9p: search open fids first"
- a7b717fa1516 btrfs: fix use-after-free after failure to create a snapshot
- 89d4cca583fc btrfs: fix deadlock between quota disable and qgroup rescan worker
- f4b2736eeb62 btrfs: don't start transaction for scrub if the fs is mounted read-only
- 7ccf5849bef7 ALSA: hda/realtek: Fix silent output on Gigabyte X570 Aorus Xtreme after reboot from Windows
- 9fc509f806a5 ALSA: hda/realtek: Fix silent output on Gigabyte X570S Aorus Master (newer chipset)
- b3625b0017a4 ALSA: hda/realtek: Add missing fixup-model entry for Gigabyte X570 ALC1220 quirks
- 730f823e3c68 ALSA: hda/realtek: Add quirk for ASUS GU603
- 586d71ddee6c ALSA: hda: realtek: Fix race at concurrent COEF updates
- 0e629052f013 ALSA: hda: Fix UAF of leds class devs at unbinding
- 303e89f94b66 ALSA: usb-audio: Correct quirk for VF0770
- b0a7836ecf13 ASoC: ops: Reject out of bounds values in snd_soc_put_xr_sx()
- e8e07c5e25a2 ASoC: ops: Reject out of bounds values in snd_soc_put_volsw_sx()
- 9e8895f1b3d4 ASoC: ops: Reject out of bounds values in snd_soc_put_volsw()
- 10007bd96b6c ASoC: hdmi-codec: Fix OOB memory accesses
- 0b8b02903173 spi: stm32-qspi: Update spi registering
- 45ba0a5fa0a6 ipc/sem: do not sleep with a spin lock held
- b8d9e0aec158 audit: improve audit queue handling when "audit=1" on cmdline
- 70caa32e6d81 selinux: fix double free of cond_list on error paths
- d63d077fc446 drm/i915: Disable DSB usage for now
- 786c4f144c7e Linux 5.15.21-xanmod1

---

### NEW RELEASE FOR PACKAGE `kernel-xanmod-lts`

Changes for 5.10.99-xm1.0:
(※ Refer commit tags to https://github.com/xanmod/linux):
- d4c19f3a9f3d Linux 5.10.99-xanmod1
- 2f25106bbced Merge tag 'v5.10.99' into 5.10
- fb063a6465f9 Linux 5.10.99
- 4889d6ee9e48 selftests: nft_concat_range: add test for reload with no element add/del
- 557727313534 cgroup/cpuset: Fix "suspicious RCU usage" lockdep warning
- f1f7d1a22fd7 net: dsa: mt7530: make NET_DSA_MT7530 select MEDIATEK_GE_PHY
- 84b76a509cc3 ext4: fix incorrect type issue during replay_del_range
- 62e46e0ffc02 ext4: fix error handling in ext4_fc_record_modified_inode()
- 764793b4a5d0 ext4: fix error handling in ext4_restore_inline_data()
- 6c5bd55e36d3 ext4: modify the logic of ext4_mb_new_blocks_simple
- 8d71fc23fcb8 ext4: prevent used blocks from being allocated during fast commit replay
- ef2053afd71e EDAC/xgene: Fix deferred probing
- 2a12faf55bae EDAC/altera: Fix deferred probing
- dd274cf85269 x86/perf: Default set FREEZE_ON_SMI for all
- 456f041e0359 perf/x86/intel/pt: Fix crash with stop filters in single-range mode
- 8c0e6a8a630e perf stat: Fix display of grouped aliased events
- 57e8859acc60 fbcon: Add option to enable legacy hardware acceleration
- 460f6b1a238d Revert "fbcon: Disable accelerated scrolling"
- 460aa9d87340 rtc: cmos: Evaluate century appropriate
- 2324f5fcdf9d tools/resolve_btfids: Do not print any commands when building silently
- 1536fafa23ac selftests: futex: Use variable MAKE instead of make
- 8f0fff8b5968 selftests/exec: Remove pipe from TEST_GEN_FILES
- 6304a613a97d bpf: Use VM_MAP instead of VM_ALLOC for ringbuf
- f744a064041c gve: fix the wrong AdminQ buffer queue index check
- 51e88e892273 nfsd: nfsd4_setclientid_confirm mistakenly expires confirmed client.
- ec4334152dae scsi: bnx2fc: Make bnx2fc_recv_frame() mp safe
- fd482f2d63db pinctrl: bcm2835: Fix a few error paths
- 752d9eafc64e pinctrl: intel: fix unexpected interrupt
- 14bc9978b486 pinctrl: intel: Fix a glitch when updating IRQ flags on a preconfigured line
- 5a45448ac95b ASoC: max9759: fix underflow in speaker_gain_control_put()
- 02f459719832 ASoC: cpcap: Check for NULL pointer after calling of_get_child_by_name
- cb5f1fbd1f22 ASoC: xilinx: xlnx_formatter_pcm: Make buffer bytes multiple of period bytes
- 56e0747d59ac ASoC: fsl: Add missing error handling in pcm030_fabric_probe
- 3e698375517d drm/i915/overlay: Prevent divide by zero bugs in scaling
- 9ea018536111 net: stmmac: ensure PTP time register reads are consistent
- 41df2da2c1f3 net: stmmac: dump gmac4 DMA registers correctly
- 114bf9350413 net: macsec: Verify that send_sci is on when setting Tx sci explicitly
- 2e7f5b6ee1a7 net: macsec: Fix offload support for NETDEV_UNREGISTER event
- 87b1c9fab6fe net: ieee802154: Return meaningful error codes from the netlink helpers
- 78b3f20c17cb net: ieee802154: ca8210: Stop leaking skb's
- 0bfe50dc5d91 net: ieee802154: mcr20a: Fix lifs/sifs periods
- 75bbda318987 net: ieee802154: hwsim: Ensure proper channel selection at probe time
- e895e067d73e spi: uniphier: fix reference count leak in uniphier_spi_probe()
- ec942d08e070 spi: meson-spicc: add IRQ check in meson_spicc_probe
- c2cf65e1008b spi: mediatek: Avoid NULL pointer crash in interrupt
- 30e05c98b99d spi: bcm-qspi: check for valid cs before applying chip select
- 6d226e8afe88 iommu/amd: Fix loop timeout issue in iommu_ga_log_enable()
- 9d9995b0371e iommu/vt-d: Fix potential memory leak in intel_setup_irq_remapping()
- b3958d315163 RDMA/mlx4: Don't continue event handler after memory allocation failure
- d3f8b927df2f RDMA/siw: Fix broken RDMA Read Fence/Resume logic.
- c7db20f5be73 IB/rdmavt: Validate remote_addr during loopback atomic tests
- 75c610212b9f RDMA/ucma: Protect mc during concurrent multicast leaves
- 371979069a57 RDMA/cma: Use correct address when leaving multicast group
- aa4ecd995f59 memcg: charge fs_context and legacy_fs_context
- 080f371d984e Revert "ASoC: mediatek: Check for error clk pointer"
- 4a9bd1e6780f IB/hfi1: Fix AIP early init panic
- 5d40f1bdad3d dma-buf: heaps: Fix potential spectre v1 gadget
- 30de3bc09978 block: bio-integrity: Advance seed correctly for larger interval sizes
- 352715593e81 mm/kmemleak: avoid scanning potential huge holes
- 7053188ddba3 mm/pgtable: define pte_index so that preprocessor could recognize it
- bce7f5d74d74 mm/debug_vm_pgtable: remove pte entry from the page table
- 2d83a7463d75 nvme-fabrics: fix state check in nvmf_ctlr_matches_baseopts()
- a0c73dbdd197 drm/amd/display: Force link_rate as LINK_RATE_RBR2 for 2018 15" Apple Retina panels
- f071d9fa8575 drm/nouveau: fix off by one in BIOS boundary checking
- 32747e01436a btrfs: fix deadlock between quota disable and qgroup rescan worker
- aa5d406153c5 ALSA: hda/realtek: Fix silent output on Gigabyte X570 Aorus Xtreme after reboot from Windows
- d4aa3a98596f ALSA: hda/realtek: Fix silent output on Gigabyte X570S Aorus Master (newer chipset)
- 3a8a8072e32b ALSA: hda/realtek: Add missing fixup-model entry for Gigabyte X570 ALC1220 quirks
- 532cde962f5f ALSA: hda/realtek: Add quirk for ASUS GU603
- 410f231fd70c ALSA: hda: realtek: Fix race at concurrent COEF updates
- a7de1002135c ALSA: hda: Fix UAF of leds class devs at unbinding
- 470bbb9cbd8f ALSA: usb-audio: Correct quirk for VF0770
- 6877f87579ed ASoC: ops: Reject out of bounds values in snd_soc_put_xr_sx()
- 038f8b7caa74 ASoC: ops: Reject out of bounds values in snd_soc_put_volsw_sx()
- a9394f21fba0 ASoC: ops: Reject out of bounds values in snd_soc_put_volsw()
- 0ff6b8050695 audit: improve audit queue handling when "audit=1" on cmdline
- f446089a268c selinux: fix double free of cond_list on error paths
