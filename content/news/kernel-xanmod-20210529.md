---
title: kernel-xanmod updates for May 29th, 2021
date: 2021-05-29
summary: Updates for the kernel-xanmod project
description: ※ This page is autogenerated
tags: ['news','kernel-xanmod']
---

### NEW RELEASE FOR PACKAGE `kernel-xanmod-lts`

Changes for 5.10.41-xanmod1.0:
(※ Refer commit tags to https://github.com/xanmod/linux):
6ffe69c Linux 5.10.41-xanmod1
93f2e30 Merge tag 'v5.10.41' into 5.10
3306991 Linux 5.10.41
b34cb7a NFC: nci: fix memory leak in nci_allocate_device
8d11e6a perf unwind: Set userdata for all __report_module() paths
53eaf28 perf unwind: Fix separate debug info files when using elfutils' libdw's unwinder
514883e KVM: x86: Defer vtime accounting 'til after IRQ handling
7706830 context_tracking: Move guest exit vtime accounting to separate helpers
5ae5e3f context_tracking: Move guest exit context tracking to separate helpers
27acfd1 bpf: No need to simulate speculative domain for immediates
c87ef24 bpf: Fix mask direction swap upon off reg sign change
4e2c7b2 bpf: Wrap aux data inside bpf_sanitize_info container

---

### NEW RELEASE FOR PACKAGE `kernel-xanmod-cacule`

Changes for 5.12.8-xanmod1_cacule.0:
(※ Refer commit tags to https://github.com/xanmod/linux):
4f422ab Linux 5.12.8-xanmod1-cacule
ad7cdd8 Merge tag 'v5.12.8' into 5.12-cacule
cfb3ea7 Linux 5.12.8
65234f5 NFC: nci: fix memory leak in nci_allocate_device
a7fcb65 KVM: x86: Defer vtime accounting 'til after IRQ handling
a4367bb context_tracking: Move guest exit vtime accounting to separate helpers
e325dc7 context_tracking: Move guest exit context tracking to separate helpers
f3ab970 bpf: No need to simulate speculative domain for immediates
4dd2aaa bpf: Fix mask direction swap upon off reg sign change
9accd53 bpf: Wrap aux data inside bpf_sanitize_info container

---

### NEW RELEASE FOR PACKAGE `kernel-xanmod-edge`

Changes for 5.12.8-xanmod1.0:
(※ Refer commit tags to https://github.com/xanmod/linux):
f7f420d Linux 5.12.8-xanmod1
e7211e5 Merge tag 'v5.12.8' into 5.12
cfb3ea7 Linux 5.12.8
65234f5 NFC: nci: fix memory leak in nci_allocate_device
a7fcb65 KVM: x86: Defer vtime accounting 'til after IRQ handling
a4367bb context_tracking: Move guest exit vtime accounting to separate helpers
e325dc7 context_tracking: Move guest exit context tracking to separate helpers
f3ab970 bpf: No need to simulate speculative domain for immediates
4dd2aaa bpf: Fix mask direction swap upon off reg sign change
9accd53 bpf: Wrap aux data inside bpf_sanitize_info container
