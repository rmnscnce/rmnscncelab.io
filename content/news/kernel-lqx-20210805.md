---
title: kernel-lqx updates for August 5th, 2021
date: 2021-08-05
summary: Updates for the kernel-lqx project
description: ※ This page is autogenerated
tags: ['news','kernel-lqx']
---

### NEW RELEASE FOR PACKAGE `kernel-lqx`

Changes for version 5.12.19-lqx4.0:
  * fix ordering of Intell remapped NVMe devices
  * add fix for multigenerational lru
   - mm: multigenerational lru: balance memory pressure between memcgs
