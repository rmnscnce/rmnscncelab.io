---
title: kernel-lqx updates for November 29th, 2021
date: 2021-11-29
summary: Updates for the kernel-lqx project
description: ※ This page is autogenerated
tags: ['news','kernel-lqx']
---

### NEW RELEASE FOR PACKAGE `kernel-lqx`

Changes for version 5.15.5-lqx3.0:
* merge 5.15.5
* add bugfix
   - scsi: sd: Fix sd_do_mode_sense() buffer length handling
* restore default kernel behavior for ACPI resource contention
   - Revert "config: Relax control over ACPI resources"
