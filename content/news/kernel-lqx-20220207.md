---
title: kernel-lqx updates for February 7th, 2022
date: 2022-02-07
summary: Updates for the kernel-lqx project
description: ※ This page is autogenerated
tags: ['news','kernel-lqx']
---

### NEW RELEASE FOR PACKAGE `kernel-lqx`

Changes for version 5.16.7-lqx1.0:
* merge 5.16.7
* replace "tsc=reliable" with "hpet=disable" into boot parameters
