---
title: kernel-lqx updates for December 2nd, 2021
date: 2021-12-02
summary: Updates for the kernel-lqx project
description: ※ This page is autogenerated
tags: ['news','kernel-lqx']
---

### NEW RELEASE FOR PACKAGE `kernel-lqx`

Changes for version 5.15.6-lqx1.0:
* merge 5.15.6
* merge 5.15.7-rc patches (ALSA low latency support)
