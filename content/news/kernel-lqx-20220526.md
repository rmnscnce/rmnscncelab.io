---
title: kernel-lqx updates for May 26th, 2022
date: 2022-05-26
summary: Updates for the kernel-lqx project
description: ※ This page is autogenerated
tags: ['news','kernel-lqx']
---

### NEW RELEASE FOR PACKAGE `kernel-lqx`

Changes for version 5.17.11-lqx1.0:
* merge 5.17.11
* disable workqueues in dm-crypt
* add RTW88 USB support
