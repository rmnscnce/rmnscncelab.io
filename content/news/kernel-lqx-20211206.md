---
title: kernel-lqx updates for December 6th, 2021
date: 2021-12-06
summary: Updates for the kernel-lqx project
description: ※ This page is autogenerated
tags: ['news','kernel-lqx']
---

### NEW RELEASE FOR PACKAGE `kernel-lqx`

Changes for version 5.15.6-lqx1.0:
* merge latest zen-kernel/5.15/fixes
    - Revert "drm/i915: Implement Wa_1508744258"
