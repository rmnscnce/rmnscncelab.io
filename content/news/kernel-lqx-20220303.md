---
title: kernel-lqx updates for March 3rd, 2022
date: 2022-03-03
summary: Updates for the kernel-lqx project
description: ※ This page is autogenerated
tags: ['news','kernel-lqx']
---

### NEW RELEASE FOR PACKAGE `kernel-lqx`

Changes for version 5.16.12-lqx1.0:
* merge 5.16.12
