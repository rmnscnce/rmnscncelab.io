---
title: kernel-xanmod updates for March 5th, 2022
date: 2022-03-05
summary: Updates for the kernel-xanmod project
description: ※ This page is autogenerated
tags: ['news','kernel-xanmod']
---

### NEW RELEASE FOR PACKAGE `kernel-xanmod-rt`

Changes for 5.15.26-rt34xm1.0:
(※ Refer commit tags to https://github.com/xanmod/linux):
- fe231d6becc6 Linux 5.15.26-rt34-xanmod1
- b6c1d0cdcb97 net: use sk_is_tcp() in more places
- 6639652684e4 Merge tag 'v5.15.26-rt34' into 5.15-rt
- aab3593b41f3 Revert "net: use sk_is_tcp() in more places"
- 7b6478f5666e Linux 5.15.26-rt34
- 038abc41d16e Merge tag 'v5.15.26' into v5.15-rt
- e9e1ef73da55 Linux 5.15.25-rt33
- 8993e6067f26 Linux 5.15.26
- 3c805fce07c9 ice: fix concurrent reset and removal of VFs
- 26bc7197f9d3 ice: Fix race conditions between virtchnl handling and VF ndo ops
- fd21a0b6da94 memblock: use kfree() to release kmalloced memblock regions
- 83f331d1debb gpio: tegra186: Fix chip_data type confusion
- a15769155440 pinctrl: k210: Fix bias-pull-up
- e3a751ee48f9 pinctrl: fix loop in k210_pinconf_get_drive()
- 92cab57ea6d7 tty: n_gsm: fix deadlock in gsmtty_open()
- 06bce5327b76 tty: n_gsm: fix wrong modem processing in convergence layer type 2
- 1bc6f3b19bc6 tty: n_gsm: fix wrong tty control line for flow control
- 50cacb783bb3 tty: n_gsm: fix NULL pointer access due to DLCI release
- 519d0b389c10 tty: n_gsm: fix proper link termination after failed open
- 4f0ab1c8a5a6 tty: n_gsm: fix encoding of control signal octet bit DV
- 1851b9a46706 riscv: fix oops caused by irqsoff latency tracer
- e0ff4dffded5 riscv: fix nommu_k210_sdcard_defconfig
- 72aa720acacf IB/qib: Fix duplicate sysfs directory name
- 7a7e1b3aeef7 tps6598x: clear int mask on probe failure
- bde6a6b111b9 staging: fbtft: fb_st7789v: reset display before initialization
- ba9efbbf6745 thermal: int340x: fix memory leak in int3400_notify()
- 00265efbd3e5 RDMA/cma: Do not change route.addr.src_addr outside state checks
- 8df508b7a44c btrfs: prevent copying too big compressed lzo segment
- d2bef2cbd3b1 driver core: Free DMA range map when device is released
- 453a82127f17 mtd: core: Fix a conflict between MTD and NVMEM on wp-gpios property
- fcd3f5906d64 nvmem: core: Fix a conflict between MTD and NVMEM on wp-gpios property
- ce94606060d7 xhci: Prevent futile URB re-submissions due to incorrect return value.
- c8b38e557414 xhci: re-initialize the HC during resume if HCE was set
- 88f69c64443f usb: dwc3: gadget: Let the interrupt handler disable bottom halves.
- 83e0190fb77c usb: dwc3: pci: Fix Bay Trail phy GPIO mappings
- e62f41a6528f usb: dwc3: pci: Add "snps,dis_u2_susphy_quirk" for Intel Bay Trail
- 943a914d3dab usb: dwc2: drd: fix soft connect when gadget is unconfigured
- 85171fbf714c USB: serial: option: add Telit LE910R1 compositions
- c331aa7e7064 USB: serial: option: add support for DW5829e
- 6db927ce66ac tracefs: Set the group ownership in apply_options() not parse_options()
- 2c775ad1fd5e USB: gadget: validate endpoint index for xilinx udc
- da514063440b usb: gadget: rndis: add spinlock for rndis response list
- f7c9fd0dff99 Revert "USB: serial: ch341: add new Product ID for CH341A"
- 27089f04fac6 ata: pata_hpt37x: disable primary channel on HPT371
- 4e508c593573 sc16is7xx: Fix for incorrect data being transmitted
- 72b0fba2dd4d iio: Fix error handling for PM
- 1f05c7568445 iio: imu: st_lsm6dsx: wait for settling time in st_lsm6dsx_read_oneshot
- c77f4ae7bd43 iio: accel: fxls8962af: add padding to regmap for SPI
- ca9d1799be68 iio: adc: ad7124: fix mask used for setting AIN_BUFP & AIN_BUFM bits
- 0cb9b2f73c18 iio: adc: tsc2046: fix memory corruption by preventing array overflow
- fe7347780298 iio: adc: men_z188_adc: Fix a resource leak in an error handling path
- 7bdf7d5f0cbd iio:imu:adis16480: fix buffering for devices with no burst mode
- 9000406481a5 tracing: Have traceon and traceoff trigger honor the instance
- 7e35b31e2cee tracing: Dump stacktrace trigger to the corresponding instance
- c8b56e51aa91 RDMA/ib_srp: Fix a deadlock
- e7a66dd26877 configfs: fix a race in configfs_{,un}register_subsystem()
- a94879d41917 bnxt_en: Increase firmware message response DMA wait time
- 27440589551f RDMA/rtrs-clt: Move free_permit from free_clt to rtrs_clt_close
- bf2cfad0c6e4 RDMA/rtrs-clt: Fix possible double free in error case
- ff999198ec21 net-timestamp: convert sk->sk_tskey to atomic_t
- d99dcdabc52a regmap-irq: Update interrupt clear register for proper reset
- 43221f446c02 gpio: rockchip: Reset int_bothedge when changing trigger
- 3c32405d6474 spi: spi-zynq-qspi: Fix a NULL pointer dereference in zynq_qspi_exec_mem_op()
- 2378f94c8d9b net/mlx5: Update log_max_qp value to be 17 at most
- 6e94d2863384 net/mlx5e: kTLS, Use CHECKSUM_UNNECESSARY for device-offloaded packets
- 95c1867704d0 net/mlx5e: MPLSoUDP decap, fix check for unsupported matches
- d4d188487ddc net/mlx5: DR, Fix the threshold that defines when pool sync is initiated
- 9703a9e2f968 net/mlx5: Fix wrong limitation of metadata match on ecpf
- f63548dd05ab net/mlx5: Fix possible deadlock on rule deletion
- 837b0d2e69e8 net/mlx5: DR, Don't allow match on IP w/o matching on full ethertype/ip_version
- 954997aeb8f2 ibmvnic: schedule failover only if vioctl fails
- 117a5a7f019e net/mlx5: DR, Cache STE shadow memory
- 6b6094db77e6 udp_tunnel: Fix end of loop test in udp_tunnel_nic_unregister()
- 4039254acbd4 surface: surface3_power: Fix battery readings on batteries without a serial number
- 062772d5cc8c net/smc: Use a mutex for locking "struct smc_pnettable"
- e96e204ee6fa netfilter: nf_tables: fix memory leak during stateful obj update
- 7d258451d345 net: mdio-ipq4019: add delay after clock enable
- 9d8097caa732 nfp: flower: Fix a potential leak in nfp_tunnel_add_shared_mac()
- 8ffb8ac34488 netfilter: nf_tables: unregister flowtable hooks on netns exit
- 2e15fa8091de net: Force inlining of checksum functions in net/checksum.h
- be2d38247657 net: ll_temac: check the return value of devm_kmalloc()
- a95ea90deb30 net/sched: act_ct: Fix flow table lookup after ct clear or switching zones
- d064d0c39405 drm/i915/dg2: Print PHY name properly on calibration error
- eae86ab32069 drm/vc4: crtc: Fix runtime_pm reference counting
- 163e50b00530 net/mlx5e: Fix wrong return value on ioctl EEPROM query failure
- 143dafa60aa7 drm/edid: Always set RGB444
- f941104aa116 openvswitch: Fix setting ipv6 fields causing hw csum failure
- 62ca33976ddc net: mv643xx_eth: process retval from of_get_mac_address
- 899e56a1ad43 gso: do not skip outer ip header in case of ipip and net_failover
- 0a9bc4179c30 tipc: Fix end of loop tests for list_for_each_entry()
- 8270e92a0e42 nvme: also mark passthrough-only namespaces ready in nvme_update_ns_info
- 6f2e0ae12aa8 net: __pskb_pull_tail() & pskb_carve_frag_list() drop_monitor friends
- c718ea4e7382 io_uring: add a schedule point in io_add_buffers()
- 8628f489b749 bpf: Add schedule points in batch ops
- 976406c5cc00 bpf: Fix a bpf_timer initialization issue
- 755d4b744056 selftests: bpf: Check bpf_msg_push_data return value
- 5d75e374eb77 bpf: Do not try bpf_msg_push_data with len 0
- 719d1c2524c8 bpf: Fix crash due to incorrect copy_map_value
- de49b0e1cf62 net/mlx5: Update the list of the PCI supported devices
- 9594d817b5eb ice: initialize local variable 'tlv'
- b3615ea66b91 ice: check the return of ice_ptp_gettimex64
- 7e8da9964437 net/mlx5: Fix tc max supported prio for nic mode
- 7efe8499cb90 hwmon: Handle failure to register sensor with thermal zone correctly
- 86da2e4a1284 bnxt_en: Fix incorrect multicast rx mask setting when not requested
- 24931b4d199e bnxt_en: Fix offline ethtool selftest with RDMA enabled
- f84bbb9893bd bnxt_en: Fix active FEC reporting to ethtool
- 1243861bc002 bnx2x: fix driver load from initrd
- 7c844c7af784 selftests: mptcp: be more conservative with cookie MPJ limits
- f76977643339 selftests: mptcp: fix diag instability
- f25ae162f4b3 mptcp: add mibs counter for ignored incoming options
- 150d1e06c4f1 mptcp: fix race in incoming ADD_ADDR option processing
- 40bbab9d4ed7 perf data: Fix double free in perf_session__delete()
- 05ef4f56173e perf evlist: Fix failed to use cpu list for uncore events
- 0b92b5f4c50b gpu: host1x: Always return syncpoint value when waiting
- 734d80b4365a Revert "i40e: Fix reset bw limit when DCB enabled with 1 TC"
- 1cfb33b338fb ping: remove pr_err from ping_lookup
- 2922aff43397 optee: use driver internal tee_context for some rpc
- 0efdc0360395 tee: export teedev_open() and teedev_close_context()
- 6c5d780469d6 netfilter: nf_tables_offload: incorrect flow offload action array size
- 144f3008524c netfilter: xt_socket: missing ifdef CONFIG_IP6_NF_IPTABLES dependency
- cb2313b216be netfilter: xt_socket: fix a typo in socket_mt_destroy()
- 49909c9f8458 CDC-NCM: avoid overflow in sanity checking
- 4b77aab7ada7 USB: zaurus: support another broken Zaurus
- 9f2d61477990 sr9700: sanity check for packet length
- 0726fca0b6cc drm/i915: Fix bw atomic check when switching between SAGV vs. no SAGV
- 1b4445e09df8 drm/i915: Correctly populate use_sagv_wm for all pipes
- 7782e3c4e539 drm/i915: Widen the QGV point mask
- 8840d963e566 drm/amdgpu: do not enable asic reset for raven2
- 70b2413ac30c drm/amdgpu: disable MMHUB PG for Picasso
- ea44fcee7e3d drm/amd: Check if ASPM is enabled from PCIe subsystem
- c00e4c01f470 drm/amd/pm: fix some OEM SKU specific stability issues
- 211b67fb5a49 drm/amd/display: Protect update_bw_bounding_box FPU code.
- 4c3644b6c96c KVM: x86/mmu: make apf token non-zero to fix bug
- 759e5dc6554d parisc/unaligned: Fix ldw() and stw() unalignment handlers
- bf0b3d61e002 parisc/unaligned: Fix fldd and fstd unaligned handlers on 32-bit kernel
- 960d474e451b vhost/vsock: don't check owner in vhost_vsock_stop() while releasing
- af091cc27e37 selinux: fix misuse of mutex_is_locked()
- 0d773aaf5a90 io_uring: disallow modification of rsrc_data during quiesce
- 7c83437fb3ae io_uring: don't convert to jiffies for waiting on timeouts
- 6d20ff677349 clk: jz4725b: fix mmc0 clock gating
- b80fbc20f334 btrfs: tree-checker: check item_size for dev_item
- 7e80846a9927 btrfs: tree-checker: check item_size for inode_item
- a6d9692cadb9 x86/ptrace: Fix xfpregs_set()'s incorrect xmm clearing
- ebeb7b73571e cgroup-v1: Correct privileges check in release_agent writes
- ffed0bf6a63d cgroup/cpuset: Fix a race between cpuset_attach() and cpu hotplug
- f89903ae99bd mm/filemap: Fix handling of THPs in generic_file_buffered_read()
- 51ffc994a85e staging: greybus: gpio: Use generic_handle_irq_safe().
- 32d49b203f5d net: usb: lan78xx: Use generic_handle_irq_safe().
- 82dad53377de mfd: ezx-pcap: Use generic_handle_irq_safe().
- 0d58f6ac0b8f misc: hi6421-spmi-pmic: Use generic_handle_irq_safe().
- 538a1e363a9b i2c: cht-wc: Use generic_handle_irq_safe().
- dfbecfdab693 i2c: core: Use generic_handle_irq_safe() in i2c_handle_smbus_host_notify().
- d0e11d4ce301 genirq: Provide generic_handle_irq_safe().
