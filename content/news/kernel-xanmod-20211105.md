---
title: kernel-xanmod updates for November 5th, 2021
date: 2021-11-05
summary: Updates for the kernel-xanmod project
description: ※ This page is autogenerated
tags: ['news','kernel-xanmod']
---

### NEW RELEASE FOR PACKAGE `kernel-xanmod-edge`+`kernel-xanmod-exptl`

Changes for 5.15.0-xm1.0 + 5.15.0-xm1.0e20211105:
(※ Refer commit tags to https://github.com/xanmod/linux):
- 05199477b9b1 Linux 5.15.0-xanmod1
- 4c1a01cd2937 hwmon: (nct6775) Add additional ASUS motherboards.
- 7d8d7c1c6cd6 hwmon: (nct6775) Support access via Asus WMI
- 620ee9237f6d hwmon: (nct6775) Use nct6775_*() function pointers in nct6775_data.
- d213ad7733ab hwmon: (nct6775) Use superio_*() function pointers in sio_data.
- 52294dfe6c0c i2c: busses: Add SMBus capability to work with OpenRGB driver control
- 7e5cd1955d9c x86/kconfig: more uarches for kernel 5.15+
- 6356ef5e280a pci: Enable overrides for missing ACS capabilities
- b3ac51ec8a68 init: wait for partition and retry scan
- e5d5e6bfe341 drivers: initialize ata before graphics
- a1b07b9ee4a9 locking: rwsem: spin faster
- 3c329979ad17 firmware: Enable stateless firmware loading
- 20dd71d8d7f6 intel_rapl: Silence rapl trace debug
- a2b5b68f1ea4 SAUCE: binder: give binder_alloc its own debug mask file
- 533c4a27ae7b SAUCE: binder: turn into module
- ca8e24c237b5 SAUCE: ashmem: turn into module
- 35f3f041b875 sysctl: add sysctl to disallow unprivileged CLONE_NEWUSER by default
- b2851fc71697 lib: zstd: Add cast to silence clang's -Wbitwise-instead-of-logical
- fdf1bbfdb160 kbuild: Add make tarzst-pkg build option
- 0c4ef9d82067 MAINTAINERS: Add maintainer entry for zstd
- 9eabe27c672c lib: zstd: Upgrade to latest upstream zstd version 1.4.10
- 5d44201d9edc lib: zstd: Add decompress_sources.h for decompress_unzstd
- a92bf084d336 lib: zstd: Add kernel-specific API
- d0fe816e6098 Documentation: amd-pstate: add amd-pstate driver introduction
- 6d3e84c31f5c cpupower: print amd-pstate information on cpupower
- 0f68a352aab7 cpupower: move print_speed function into misc helper
- 2e3c1f40a15f cpupower: enable boost state support for amd-pstate module
- e843b0dcbe9d cpupower: add amd-pstate sysfs definition and access helper
- d391c3172281 cpupower: add the function to get the sysfs value from specific table
- d5bc8e3e262c cpupower: initial AMD P-state capability
- 3223a43bae34 cpupower: add the function to check amd-pstate enabled
- 2be733db76f7 cpupower: add AMD P-state capability flag
- b89d41fa5e10 cpufreq: amd: add amd-pstate performance attributes
- dd80c527df00 cpufreq: amd: add amd-pstate frequencies attributes
- 9f1e5af9f6c1 cpufreq: amd: add boost mode support for amd-pstate
- de13cc00ce1d cpufreq: amd: add trace for amd-pstate module
- a97ebea71a98 cpufreq: amd: add acpi cppc function as the backend for legacy processors
- 57448d13ce9a cpufreq: amd: add fast switch function for amd-pstate
- 7d9f5592ddfd cpufreq: amd: introduce a new amd pstate driver to support future processors
- 507e3fa01595 ACPI: CPPC: add cppc enable register function
- 281d2f125652 ACPI: CPPC: Check present CPUs for determining _CPC is valid
- 006bf956420e ACPI: CPPC: implement support for SystemIO registers
- 5abba2f85d7e x86/msr: add AMD CPPC MSR definitions
- a00d065f4b06 x86/cpufreatures: add AMD Collaborative Processor Performance Control feature flag
- 1243cc77acda char/lrng: add power-on and runtime self-tests
- 64564cc8c19a char/lrng: add interface for gathering of raw entropy
- 1fba41ef7445 char/lrng: add SP800-90B compliant health tests
- 161114f6a2c6 char/lrng: add Jitter RNG fast noise source
- 3c7c2e767ee6 crypto: move Jitter RNG header include dir
- 2797709be3eb char/lrng: add kernel crypto API PRNG extension
- e9384fc48ad9 char/lrng: add SP800-90A DRBG extension
- 8f2ac07c0725 crypto: DRBG - externalize DRBG functions for LRNG
- 7ce9102c2868 char/lrng: add common generic hash support
- 6b2cceb51b0e char/lrng: add switchable DRNG support
- e45b8648e637 char/lrng: sysctls and /proc interface
- f7f12c5e2ad2 char/lrng: allocate one DRNG instance per NUMA node
- ab993f0e8e8f drivers: Introduce the Linux Random Number Generator
- 837b0aeffa60 net-tcp_bbr: v2: Fix missing ECT markings on retransmits for BBRv2
- b8fa56fb9f35 net-tcp_bbr: v2: don't assume prior_cwnd was set entering CA_Loss
- 699194ffc5ec net-tcp_bbr: v2: remove cycle_rand parameter that is unused in BBRv2
- 19bb512064ef net-tcp_bbr: v2: remove field bw_rtts that is unused in BBRv2
- b02ffd2d6287 net-tcp_bbr: v2: remove unnecessary rs.delivered_ce logic upon loss
- 9b7be0b3c71d net-tcp_bbr: v2: BBRv2 ("bbr2") congestion control for Linux TCP
- 65e4da0782e4 net-tcp: add fast_ack_mode=1: skip rwin check in tcp_fast_ack_mode__tcp_ack_snd_check()
- 39d36331cfe3 net-tcp: re-generalize TSO sizing in TCP CC module API
- 3110f23f540d net-tcp: add new ca opts flag TCP_CONG_WANTS_CE_EVENTS
- 30e316fb1b40 net-tcp_bbr: v2: set tx.in_flight for skbs in repair write queue
- d50e05bdceab net-tcp_bbr: v2: adjust skb tx.in_flight upon split in tcp_fragment()
- 293e82ac9754 net-tcp_bbr: v2: adjust skb tx.in_flight upon merge in tcp_shifted_skb()
- 400bc7e73e85 net-tcp_bbr: v2: factor out tx.in_flight setting into tcp_set_tx_in_flight()
- 61a0a7585d9a net-tcp_bbr: v2: introduce ca_ops->skb_marked_lost() CC module callback API
- 6aca161876b5 net-tcp_bbr: v2: export FLAG_ECE in rate_sample.is_ece
- b47bb01ca950 net-tcp_bbr: v2: count packets lost over TCP rate sampling interval
- 8166fcfbbd67 net-tcp_bbr: v2: snapshot packets in flight at transmit time and pass in rate_sample
- 32b38b7a8bfb net-tcp_bbr: v2: shrink delivered_mstamp, first_tx_mstamp to u32 to free up 8 bytes
- 2944f3f4d620 net-tcp_rate: account for CE marks in rate sample
- 7222702d8445 net-tcp_rate: consolidate inflight tracking approaches in TCP
- a1b8bfc95fd8 net-tcp_bbr: broaden app-limited rate sample detection
- 4afde7d1d43f mm/vmscan: add sysctl knobs for protecting the working set
- ad80c3b998a4 winesync: Introduce the winesync driver and character device
- 304b10fb971d futex: Add entry point for FUTEX_WAIT_MULTIPLE (opcode 31)
- 6d1b96996072 futex,x86: Wire up sys_futex_waitv()
- 0552eed197f1 futex: Implement sys_futex_waitv()
- 4fdad1cc6d6b clockevents, hrtimer: Make hrtimer granularity and minimum hrtimeout configurable in sysctl. Set default granularity to 100us and min timeout to 500us
- 5e67c25bb8e6 time: Don't use hrtimer overlay when pm_freezing since some drivers still don't correctly use freezable timeouts.
- 36e2473399cf hrtimer: Replace all calls to schedule_timeout_uninterruptible of potentially under 50ms to use schedule_msec_hrtimeout_uninterruptible
- c94909f9892e hrtimer: Replace all calls to schedule_timeout_interruptible of potentially under 50ms to use schedule_msec_hrtimeout_interruptible.
- 7a07c55b886b hrtimer: Replace all schedule timeout(1) with schedule_min_hrtimeout()
- b1119134900a timer: Convert msleep to use hrtimers when active.
- de3d10622c99 time: Special case calls of schedule_timeout(1) to use the min hrtimeout of 1ms, working around low Hz resolutions.
- 8e6b86a0ed4a hrtimer: Create highres timeout variants of schedule_timeout functions.
- 1c0343c72b75 XANMOD: fair: Remove all energy efficiency functions
- 9232ff82bbc5 XANMOD: Makefile: Turn off loop vectorization for GCC -O3 optimization level
- acecd1e82b0a XANMOD: init/Kconfig: Enable -O3 KBUILD_CFLAGS optimization for all architectures
- 5973084b3c95 XANMOD: lib/kconfig.debug: disable default CONFIG_SYMBOLIC_ERRNAME and CONFIG_DEBUG_BUGVERBOSE
- 70dff0736d29 XANMOD: scripts: disable the localversion "+" tag of a git repo
- 6bf377d46ef3 XANMOD: cpufreq: tunes ondemand and conservative governor for performance
- 9879bb447b5a XANMOD: mm/vmscan: vm_swappiness = 30 decreases the amount of swapping
- 51fb6e90d186 XANMOD: sched/autogroup: Add kernel parameter and config option to enable/disable autogroup feature by default
- d009ad96d156 XANMOD: dcache: cache_pressure = 50 decreases the rate at which VFS caches are reclaimed
- fb0e76989bb3 XANMOD: kconfig: add 500Hz timer interrupt kernel config option
- d0bc09370b4a XANMOD: block: set rq_affinity to force full multithreading I/O requests
- 7c013f8d5c9e XANMOD: block, bfq: change BLK_DEV_ZONED depends to IOSCHED_BFQ
- af2a86239ff9 XANMOD: elevator: set default scheduler to bfq for blk-mq
- 8bb7eca972ad Linux 5.15
