---
title: kernel-lqx updates for July 13th, 2021
date: 2021-07-13
summary: Updates for the kernel-lqx project
description: ※ This page is autogenerated
tags: ['news','kernel-lqx']
---

### NEW RELEASE FOR PACKAGE `kernel-lqx`

Changes for version 5.12.16-lqx2.0:
  * revert CPU selection tuning
