---
title: kernel-xanmod updates for October 4th, 2021
date: 2021-10-04
summary: Updates for the kernel-xanmod project
description: ※ This page is autogenerated
tags: ['news','kernel-xanmod']
---

### NEW RELEASE FOR PACKAGE `kernel-xanmod-edge`+`kernel-xanmod-exptl`

Changes for 5.14.9-xm2.0 + 5.14.9-xm2.0e20210920:
(※ Refer commit tags to https://github.com/xanmod/linux):
* 1d25d6095fa0 Linux 5.14.9-xanmod2
* f164cc43ea1a zstd: fix fall-through warnings
* 90383d79db7c futex2: Add sysfs entry for syscall numbers
* 442d05a80b7e kernel: Enable waitpid() for futex2
* e5a1578d65ce docs: locking: futex2: Add documentation
* fa85b67049cc futex2: Add compatibility entry point for x86_x32 ABI
* 5f5c48319897 futex2: Implement requeue operation
* 9970c3df4edb futex2: Implement vectorized wait
* 1ee81ce10706 futex2: Add support for shared futexes
* ea14247af977 futex2: Implement wait and wake functions
* 6afbd4f8cd3c futex: Implement mechanism to wait on any of several futexes
* 12aea22c5b44 futex*: remove FUTEX_WAIT_MULTIPLE operation and futex2 patchset
