---
title: "How to self-sign custom kernel image and its modules for UEFI secure boot"
description: "(for Fedora Linux)"
date: 2021-09-20
TocOpen: false
category: 'tutorials'
tags: ['fedora','tutorials','kernel']
---
## Requirements (install with `dnf`)
- `shim`
- `sbsigntools`
- `mokutil`
- `nss-tools`
- `openssl`
- `kernel*-devel`

## Steps
- Create and `cd` into a working directory, which in this tutorial will be named `kernel-sign`:

```bash
mkdir kernel-sign/ && cd $_
```
- Prepare an environment variable that contains the kernel version that wants to be signed, which in this tutorial will be called `sign_kver`:

`kernel.env`:

```bash
sign_kver="<kernel-version>" # example: 5.13.8-200.fc34.x86_64
export ${sign_kver}
```

And then load the file:

```bash
. ./kernel.env
```

- Grab the `mod-sign.sh` script from the Fedora DistGit repository for `kernel`:

Run in terminal:

```
curl -O https://src.fedoraproject.org/rpms/kernel/raw/rawhide/f/mod-sign.sh
chmod +x ./mod-sign.sh
```

- Copy `scripts/sign-file` from the installed kernel sources in `/usr/src/kernels` for the corresponding kernel version into a folder called `scripts`:

Run in terminal:

```
mkdir ./scripts
cp -vx /usr/src/kernels/${sign_kver}/scripts/sign-file ./scripts/
```

- Prepare a NSS certificate database:

Run in terminal:

```bash
mkdir -p nss/
certutil -N -d ./nss/
# create your password
```

- Generate the key:

Run in terminal:

```bash
efikeygen -d nss -S -n kernel-sign -c "CN=Kernel self-signing key"
```

- Export the DER certificate from the database:

Run in terminal:

```bash
certutil -d ./nss/ -L -n kernel-sign -r > cert_pub.der
# enter your password 
```

- Clone the DER certificate into a PEM using OpenSSL:

Run in terminal:

```bash
openssl x509 -in cert_pub.der -inform DER -outform PEM -out cert_pub.pem
```

- Export the private key into a PKCS#12 file, and then convert it into a plaintext file with OpenSSL:

Run in terminal:

```bash
pk12util -d nss -n kernel-sign -o key_priv.p12
# enter your password
openssl pkcs12 -info -in key_priv.p12 -nodes | head -n 32 | tail -n 28 > key_priv
```

- Import public key into MOK using `mokutil`, enter the password for shim, and then reboot the machine:

Run in terminal:

```
sudo mokutil -i ./cert_pub.der
sudo systemctl reboot
```

- Enroll the key with the menu that appeared (use the same password as what you've entered in `mokutil`):

![MokEnroll](https://pagure.io/rmnscnce/vault/issue/raw/files/e914ff8566cd8ba324f5dfe5db7ce21b0bb7306d87c3866f16b50786d3dcfcfe-mokmanager.png)

- Boot back into Fedora

- Load the environment variable file, and get the kernel image into the directory:

Run in terminal:

```
. ./kernel.env
cp -ax /usr/lib/modules/${sign_kver}/vmlinuz ./
```

- Sign the kernel image:

Run in terminal:

```bash
sbsign --key key_priv --cert cert_pub.pem ./vmlinuz --output ./vmlinuz.signed
```

- Get the shim file for the bootloader, which in this case will be the one inside the Fedora GRUB2 directory inside the default ESP

Run in terminal:

```bash
sudo cp -vx /boot/efi/EFI/fedora/shimx64.efi /boot/efi/EFI/fedora/shimx64.efi.bak # back up first
sudo cp -vx /boot/efi/EFI/fedora/shimx64.efi ./shimx64.efi
chmod go+rw ./shimx64.efi
```

- Sign the shim file with the key using `sbsign`

Run in terminal:
```bash
sbsign --key key_priv --cert cert_pub.pem ./shimx64.efi --output ./shimx64.efi.signed
```

- Get the unsigned kernel modules:

Run in terminal:

```bash
cp -rvx /usr/lib/modules/${sign_kver}/kernel ./kernel/
cp -rvx ./kernel/ ./kernel.presign/ # back up
```

- Sign the kernel modules:

Run in terminal:

```bash
./mod-sign.sh key_priv cert_pub.der ./kernel/
```

- Copy the signed image and modules and shim back to their places:

Run in terminal:

```
sudo cp -vx ./vmlinuz.signed /usr/lib/modules/${sign_kver}/vmlinuz
sudo cp -vx ./vmlinuz.signed /boot/vmlinuz-${sign_kver}
sudo cp -rvx ./kernel/ /usr/lib/modules/${sign_kver}/
sudo cp -vx ./shimx64.efi.signed /boot/efi/EFI/fedora/shimx64.efi
```

- Install the kernel image into the bootloader

Run in terminal:

```
sudo kernel-install -v add ${sign_kver} /usr/lib/modules/${sign_kver}/vmlinuz
```

- Done! Now you have successfully self-signed the custom kernel for secure boot. Now you may reboot into the new signed custom kernel


## Additional notes
- For subsequent signing (for kernel updates) you will only need to follow the signing steps. No need to regenerate keys. Adjust the `kernel.env` file accordingly

- You can use `akmods-secureboot` and `kmodtool-secureboot` normally using the keys generated using those steps

