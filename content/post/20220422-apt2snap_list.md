---
title: "Snap installers in Ubuntu APT repositories"
description: "Here are the packages in Ubuntu repos that install their Snap counterparts"
date: 2022-04-22
TocOpen: false
category: 'guides'
tags: ['ubuntu']
---
Last updated: April 22, 2022

| Package name     | Available in latest Ubuntu? | Oldest supported Ubuntu | When did it become a Snap installer?                                                                                  |
| ---------------- | --------------------------- | ----------------------- | --------------------------------------------------------------------------------------------------------------------- |
| lxd              | Yes                         | 20.04LTS ("focal")      | [18.10 ("cosmic")](https://launchpadlibrarian.net/388070000/lxd_0.1_amd64.changes)                                    |
| chromium-browser | Yes                         | 20.04LTS ("focal")      | [19.10 ("eoan")](https://launchpadlibrarian.net/427896917/chromium-browser_75.0.3770.80-0ubuntu1~snap1_amd64.changes) |
| snapcraft        | Yes                         | 20.04LTS ("focal")      | [19.10 ("eoan")](https://launchpadlibrarian.net/441567765/snapcraft_3.0_amd64.changes)                                |
| ~~maas~~         | No                          | 20.04LTS ("focal")      | [20.04LTS ("focal")](https://launchpadlibrarian.net/465541122/maas_0.1_amd64.changes)                                 |
| firefox          | Yes                         | 22.04LTS ("jammy")      | [22.04LTS ("jammy")](https://launchpadlibrarian.net/587751659/firefox_1snap1-0ubuntu1_amd64.changes)                  |

##### Data sourced from [Ubuntu Packages](https://packages.ubuntu.com/) and [Canonical Launchpad](https://launchpad.net/)
