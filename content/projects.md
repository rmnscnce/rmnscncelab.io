---
title: Projects
description: "List of stuff that I'm working — and have worked — on, classified with the \"traffic light\" scale"
date: 2021-09-20
---
## 🟢 Actively maintaining

These projects are being maintained actively by me and have healthy amount of release in a given time frame

### Liquorix kernel (unofficial) Fedora Linux port

[Link to project](https://copr.fedorainfracloud.org/coprs/rmnscnce/kernel-lqx/)

Also called `kernel-lqx`, this one started around the time I moved (read: refuged) from Bedrock Linux to Fedora Linux. During my time of using Bedrock Linux, I used to use Liquorix kernel, a (highly) popular custom kernel which is primarily built for Debian with no Fedora Linux support. It was the only way I could sanely play games on my laptop which usually just overheats on default Fedora kernel. So then I thought, "Hey, the Linux kernel and *all* of its derivatives are free software and there's this Copr thing which allows people to freely build free software RPMs for Fedora Linux and distribute it on their servers," which resulted in days of writing, testing, and debugging RPM spec file, which — ultimately, resulted in a stable kernel RPM spec template that I use now to build and maintain [another custom kernel port](#xanmod-kernel-unofficial-fedora-linux-port).

Fun fact: This one is my longest-standing project as of now.

### XanMod kernel (unofficial) Fedora Linux port

[Link to project](https://copr.fedorainfracloud.org/coprs/rmnscnce/kernel-xanmod/)

Remember my [first attempt](#liquorix-kernel-unofficial-fedora-linux-port) at custom kernel port for Fedora Linux? Turns out, there's another custom kernel — also primarily built for Debian without Fedora Linux support, called XanMod kernel. It's also a popular one, having itself featured for some times on Phoronix for kernel benchmarking tests. Nothing is really different between maintaining this and the other custom kernel port, other than the fact that it has 5 different branches maintained reflecting the different options that are available for the custom kernel, which is totally awesome. Long story short, I managed to port the variants one-by-one into RPM packages for Fedora Linux, and here we are, living in the world where XanMod kernel has unofficial builds for Fedora Linux.

Fun fact: This is my most popular project as of now, with over 2000 repo downloads (basically the amount of times a user decided to add the repository to their software package sources of choice) and 16(!) upvotes on Fedora Copr (probably the most-upvoted project throughout Fedora Copr). If you're a user and just happen to read this, thank you!

### Fedora Linux packaging

Although I'm not a packager for Fedora Linux *yet*, I have been preparing and continuously polishing (albeit in a slower moving pace) the RPM spec files for things that I think should get into Fedora Linux as officially shipped packages. Among those things are:

- [Pebbles](https://github.com/SubhadeepJasu/pebbles), an advanced GTK3 calculator app written in Vala using elementaryOS design guidelines, and
- [amdctl](https://github.com/kevinlekiller/amdctl), a really awesome program that enables P-State voltage and clock speed configuration for several AMD CPUs on Linux

Fun fact: amdctl is quite an important program for me because it allows me to undervolt my laptop CPU so it stays on a safe temperature range.

### Ultramarine Linux

[Link to project](https://ultramarine-linux.org/)

This is where it goes to the ambitious and slower(!) side of things. Ultramarine Linux is a Fedora Remix distribution project that was envisioned after hijacking the `#home` Fedora Linux Discord chat for 1~2 days(!) with the (very technical) discussion of porting a lesser-known Linux desktop environment called Cyber from the [CyberOS project](https://getcyberos.org/) into Fedora Linux.

The distro ships with Cyber Desktop, Cutefish Desktop, and Budgie Desktop. It also has out-of-the-box Flathub support and DNF repository entries that enable additional software support that may enhance the experience of using the distro, such as codecs, signed akmods support, and optional repositories for development that can be easily enabled with a single line of command.

Fun fact: Fun facts are actually optional

## 🟡 Passively maintaining

These are the ones that get the lesser attention because of the scarce of time or motivation, and only get the attention after accidentally seeing the notification mail coming into the inbox

### ntfs3 module

[Link to project](https://github.com/rmnscnce/ntfs3)


## 🔴 No longer maintaining

**TBA**

Isn't it ironic how a list of unmaintained projects is unmaintained? I'm just too lazy to write a long list of my sins of drive-by project maintenance that got started out of spite
